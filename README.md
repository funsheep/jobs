# jobs Platform

## Introduction
This platforms intended use is for projects that use different threads for different contexts (e.g. one thread to do all the networking, one thread for all of UI, etc.).
Instead of passing the different thread instances around or every time creating a new API for context execution, with this platform a string based context identifier this
the only thing needed (to pass around).

Especially, for plugin-based projects, this is a major simplification and standardization.

## Usage

### My First Job (Runnable)

Adding your first job to the jobs platform is pretty simple. Just implement a `Runnable` and schedule it through the central hub `Jobs`.

```java
Runnable myFirstJob = () ->
{
	System.out.println("Hello from my first job");
}

...

Jobs.runJob("myFirstJob", myFirstJob, "network");
```

This will execute the `myFirstJob` runnable in a thread named `network` as soon as possible one time. Hereby the platform takes care of the thread creation, handling and
its disposal. If you add a new job later on to the `network` thread, if it no longer exists, it will be recreated by the platform. Therefore, it makes context-based execution
very easy. Just name the context that should execute something and your are all set.

###  Maven + Gradle Setup

Maven dependency:

	<dependency>
		<groupdId>cc.renken</groupId>
		<artifactId>jobs</artifactId>
		<version>1.0.0</version>
	</dependency>
	
Gradle dependency:

	dependencies {
		implementation 'cc.renken:jobs:1.0.0'
	}

### IDoJob

For more control and more scheduling options, the `IDoJob` interface should be implemented. Using this interface, e.g. allows the scheduling of jobs at a fixed rate.
Furthermore, it allows very fine grained control over the execution. It allows to provide information about the "finished" state of the job to the jobs platform.
Jobs that are finished, are no longer executed. Furthermore, for convenience and user feedback, the interface also allows to return the progress of a job.

For convenience, an abstract implementation where one needs to add custom logic only, is provided with the `ADoJob` class.

```java
ADoJob job = new ADoJob("mySecondJob")
{

	private int count = 10;

	@Override
	public void doJob()
	{
		System.out.println("Hello from my first job");
		this.count--;
		if (this.count == 0)
			this.shutdown();
	}
	
};

Jobs.loopRun(job, 1000, Jobs.MAIN_THREAD);
```

The execution will be done at a fixed rate of 1000 milliseconds (= 1 second). This job will be executed 10 times after which the thread sets itself to "finished" by
calling `ADoJob#shutdown()`. Then the jobs platform will stop the execution.

### Shutdown

Imagine, the platform is running with some jobs. Now the user wants to exit the program. Then all the jobs have to be shutdown. All of this is taken care of by the platform.
To shutdown the entire platform, one has to call

```java
Jobs.shutdown();
```

This will interrupt all running jobs. Then on every **not** finished job, `IJob#shutdown()` is called. This gives the job the opportunity to exit gracefully and cleanup
any held resources. Then the platform exits.

### Wait for Shutdown ##

To prevent blocking jobs from exiting the program, all threads created by the platform are created as *daemon*-threads. Unfortunately, if the main method of a java program
exits and there are *daemonized* threads left only, then the jvm will also exit. To prevent this, the Jobs platform allows to wait until someone has called `Jobs.shutdown()`.

```java
public static final void main(String[] args)
{
	//... start your jobs here
	
	Jobs.waitForShutdown();
}
```

The `waitForShutdown()` method will block, until `Jobs.shutdown()` is called. This is different to having non `daemonized` threads. Instead, even if no job is scheduled,
the jvm is not exited. Even if there is any thread blocked by a job (that e.g. has a deadlock), the jvm will exit and the program will end. Therefore, using this pattern, the
jobs platform guarantees control over the shutdown process of a program.

## Advanced Stuff

### Monitor Custom Thread

At times, a project uses the usual threads or executor services in the java.concurrent package. However, still one may want to monitor and even manage! this thread through
the jobs platform. With the `JobStub` this is possible. The threads logic has to be adapted in a minimal way - shown below. 

A common thread implementation looks similar to this:

```java
public void run()
{
	//do some initialization
	while (!this.isInterrupted())
	{
		//do stuff, possibly blocking
	}
	//do cleanup
}

public void shutdown()
{
	this.interrupt();
}
```

To integrate this thread implementation into the Jobs platform, the only thing to do, is to change the constraint of the while loop to use `JobStub#isFinished()` method.
Even better, if `JobStub` knows the actual executing thread, on a platform shutdown, the thread will be interrupted to exit even if it is blocked by some (e.g. IO-)operation.

### Synchronization
Usually, one uses threads to execute blocking (or computing intensive) methods without blocking other executions. In that case, results have to be handed over to other threads.
To ease things, the jobs platform provides an `IDoJob` implementation for this use case (producer/consumer pattern).

The producer would be implemented as a child from `AComputeDoJob`. The only thing needed here is, to implement the logic into the `doJob()` method.
The consumer then can use the `get()` method to retrieve the result.

## Misc

### JobInfo

The JobInfo type provides a pretty printer for all job related information.