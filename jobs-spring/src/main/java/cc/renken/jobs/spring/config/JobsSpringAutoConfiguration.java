/**
 * 
 */
package cc.renken.jobs.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author edvrenke
 *
 */
@Configuration
@ComponentScan("cc.renken.jobs.spring")
public class JobsSpringAutoConfiguration
{
}
