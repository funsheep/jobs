/**
 * 
 */
package cc.renken.jobs.spring.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Configuration;

/**
 * @author edvrenke
 *
 */
@Configuration
public class JobsConfiguration extends ExecutorConfigurationBean
{
	//FIXME add generation of configuration metadata to build pipeline (also for other projects like eventbus)
	private final Map<String, ExecutorConfigurationBean> namedExecConfig = new HashMap<String, ExecutorConfigurationBean>();
	
	public void setNamedExecConfig(Map<String, ExecutorConfigurationBean> namedExecConfigs)
	{
		this.namedExecConfig.clear();
		this.namedExecConfig.putAll(namedExecConfigs);
	}
	
	public Map<String, ExecutorConfigurationBean> getNamedExecConfig()
	{
		return Collections.unmodifiableMap(this.namedExecConfig);
	}

}
