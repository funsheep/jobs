/**
 * 
 */
package cc.renken.jobs.spring.config;

import java.util.concurrent.TimeUnit;

import cc.renken.jobs.ExecutorConfiguration;

/**
 * @author edvrenke
 *
 */
public class ExecutorConfigurationBean
{

	private final ExecutorConfiguration defaultExecConfig = new ExecutorConfiguration();

	public int getCorePoolSize()
	{
		return this.defaultExecConfig.getCorePoolSize();
	}
	
	public void setCorePoolSize(int coreSize)
	{
		this.defaultExecConfig.setCorePoolSize(coreSize);
	}
	
	public void setDaemonizeThreads(boolean daemonize)
	{
		this.defaultExecConfig.setDaemonizeThreads(daemonize);
	}
	
	public boolean getDaemonizeThreads()
	{
		return this.defaultExecConfig.getDaemonizeThreads();
	}
	
	public long getDefaultTaskTimeout()
	{
		return this.defaultExecConfig.getDefaultTaskTimeout();
	}
	
	public void setDefaultTaskTimeout(long defaultTaskTimeout)
	{
		this.defaultExecConfig.setDefaultTaskTimeout(defaultTaskTimeout);
	}
	
	public TimeUnit getDefaultTaskTimeoutTimeUnit()
	{
		return this.defaultExecConfig.getDefaultTaskTimeoutTimeUnit();
	}
	
	public void setDefaultTaskTimeoutTimeUnit(TimeUnit defaultTaskTimeoutTimeUnit)
	{
		this.defaultExecConfig.setDefaultTaskTimeoutTimeUnit(defaultTaskTimeoutTimeUnit);
	}

}
