/**
 * 
 */
package cc.renken.jobs;

import java.time.Duration;
import java.time.Instant;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import cc.renken.jobs.concurrent.TimeoutRunnable;


/**
 * @author renkenh
 *
 */
abstract class ADoJobHandle<V, J extends IJob> implements IJobHandle<V>, TimeoutRunnable
{
	
	private class Result
	{
		private final V value;
		private final Exception error;
		
		private Result(V value)
		{
			this.value = value;
			this.error = null;
		}

		private Result(Exception error)
		{
			this.value = null;
			this.error = error;
		}
		
		private boolean isError()
		{
			return this.error != null;
		}
	}

	protected final J job;
	private final long delay;
	private final long interExecTime;
	private final long timeout;
	private final TimeUnit unit;
	private final String thread;
	private Future<?> future;
	
	private final ReentrantLock lock = new ReentrantLock();
	private final Condition waitForFinish = this.lock.newCondition();
	
	private final Deque<Result> results = new LinkedList<>();


	private volatile boolean isShutdown = false;

	
	public ADoJobHandle(J job, String thread, long delay, long interExecTime, long timeout, TimeUnit unit)
	{
		this.job = job;
		this.delay = delay;
		this.interExecTime = interExecTime;
		this.timeout = timeout;
		this.thread = thread;
		this.unit = unit;
	}
	
	void setFuture(Future<?> future)
	{
		this.future = future;
	}

	
	@Override
	public boolean isDone()
	{
		return Jobs.isShutdown || this.isShutdown || this.job.isfinished();
	}

	@Override
	public IJob getJob()
	{
		return this.job;
	}

	@Override
	public long getDelay()
	{
		return this.delay;
	}

	@Override
	public String getThread()
	{
		return this.thread;
	}

	@Override
	public long timeout()
	{
		return this.timeout;
	}

	@Override
	public TimeUnit timeoutTimeunit()
	{
		return this.timeUnit();
	}

	@Override
	public long interExecutionTime()
	{
		return this.interExecTime;
	}

	@Override
	public TimeUnit timeUnit()
	{
		return this.unit;
	}

	@Override
	public void shutdown()
	{
		this.isShutdown = true;
		this.future.cancel(true);
		this.lock.lock();
		try
		{
			this.waitForFinish.signalAll();
		}
		finally
		{
			this.lock.unlock();
		}
	}

	@Override
	public void cancel()
	{
		this.isShutdown = true;
		this.future.cancel(false);
		this.lock.lock();
		try
		{
			this.waitForFinish.signalAll();
		}
		finally
		{
			this.lock.unlock();
		}
	}

	@Override
	public String toString()
	{
		return JobInfoUtil.toString(this);
	}

	/**
	 * This implementation is the producer. From the {@link #doJob()} method call this method to handover a result.
	 * <code>null</code> also is a valid result!
	 * The given result will be provided to the consumer through the {@link #get()} method.
	 * @param object The result. <code>null</code> is a valid result.
	 */
	protected final void finished(V value)
	{
		this.finished(new Result(value));
	}

	/**
	 * Call this method if the {@link #doJob()} method which should have produced a result ran into some error and produced an exception. Then this method allows the
	 * handover of the exception to the consumer.
	 * @param e The exception that occurred.
	 */
	protected final void finishedWithError(Exception e)
	{
		this.finished(new Result(e));
	}

	private final void finished(Result result)
	{
		this.lock.lock();
		try
		{
			this.results.add(result);
			this.waitForFinish.signalAll();
		}
		finally
		{
			this.lock.unlock();
		}
	}

	public final V tryGet() throws Exception
	{
		this.lock.lock();
		try
		{
			if (!this.results.isEmpty())
			{
				Result result = this.results.removeFirst();
				if (result.isError())
					throw result.error;
				return result.value;
			}
			return null;
		}
		finally
		{
			this.lock.unlock();
		}
	}

	@Override
	public final V get(final long timeout) throws TimeoutException, Exception
	{
		if (Thread.currentThread().getName().equals(this.thread))
			throw new IllegalStateException("The job executing thread "+this.thread+" calls this method. This would result in a deadlock. Instead non-blocking #tryGet() should be used.");

		this.lock.lock();
		try
		{
			Instant last = Instant.now();
			long toWait = timeout;
			while (this.results.isEmpty() && !this.isShutdown)
			{
				if (timeout > 0 && (toWait <= 0 || !this.waitForFinish.await(toWait, this.timeUnit())))
					throw new TimeoutException("Job execution timeout");
				else if (timeout <= 0)
					this.waitForFinish.await();
				if (Thread.currentThread().isInterrupted())
					throw new TimeoutException("Job execution timeout");
				Instant now = Instant.now();
				toWait -= Duration.between(last, now).get(this.timeUnit().toChronoUnit());
				last = now;
			}
			if (this.isShutdown)
				throw new TimeoutException("Job was shutdown.");
			Result result = this.results.removeFirst();
			if (result.isError())
				throw result.error;
			return result.value;
		}
		catch (InterruptedException ie)
		{
			throw new TimeoutException("Job execution timeout");
		}
		finally
		{
			this.lock.unlock();
		}
	}
	
}
