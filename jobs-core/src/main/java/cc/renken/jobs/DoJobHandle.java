/**
 * 
 */
package cc.renken.jobs;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


/**
 * @author renkenh
 *
 */
class DoJobHandle extends ADoJobHandle<Void, IDoJob>
{

	public DoJobHandle(IDoJob job, String thread, long delay, long interExecTime, long timeout, TimeUnit unit)
	{
		super(job, thread, delay, interExecTime, timeout, unit);
	}


	@Override
	public void run()
	{
		try
		{
			this.job.doJob();
			this.finished(null);
		}
		catch (InterruptedException ie)
		{
			this.finishedWithError(new TimeoutException("Job execution timeout"));
		}
		catch (Exception e)
		{
			this.finishedWithError(e);
		}
	}

}
