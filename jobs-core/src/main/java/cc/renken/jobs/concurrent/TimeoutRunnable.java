/**
 * 
 */
package cc.renken.jobs.concurrent;

import java.util.concurrent.TimeUnit;

/**
 * @author renkenh
 *
 */
public interface TimeoutRunnable extends Runnable
{

	public long timeout();
	
	public TimeUnit timeoutTimeunit();
}
