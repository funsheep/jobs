/**
 * 
 */
package cc.renken.jobs.concurrent;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;


/**
 * ExecutorService that monitors given tasks and interrupts them on a specified timeout. Further monitoring is not implemented.
 * If the monitored thread then still not ends itself - no further action is taken.
 * 
 * Within the constructor a default timeout can be set. This timeout is applied to any given task.
 * However, if the task implements the {@link TimeoutRunnable} interface, the default timeout is overridden with the values provided by the task.
 * 
 * A timeout value of zero or less disables the timeout mechanism. 
 * 
 * @author renkenh
 * @author Edward Dale (https://stackoverflow.com/questions/2758612/executorservice-that-interrupts-tasks-after-a-timeout)
 */
public class TimeoutThreadPoolExecutor extends ScheduledThreadPoolExecutor
{


	private long defaultTimeout;
	private TimeUnit defaultTimeoutUnit;

	private final ScheduledExecutorService timeoutExecutor = Executors.newSingleThreadScheduledExecutor();
	private final ConcurrentMap<Runnable, ScheduledFuture<?>> timeoutTasks = new ConcurrentHashMap<>();


	public TimeoutThreadPoolExecutor(int corePoolSize, long defaultTimeout, TimeUnit defaultTimeoutUnit)
	{
		super(corePoolSize);
		this.defaultTimeout = defaultTimeout;
		this.defaultTimeoutUnit = defaultTimeoutUnit;
	}

	public TimeoutThreadPoolExecutor(int corePoolSize, ThreadFactory threadFactory, long defaultTimeout, TimeUnit defaultTimeoutUnit)
	{
		super(corePoolSize, threadFactory);
		this.defaultTimeout = defaultTimeout;
		this.defaultTimeoutUnit = defaultTimeoutUnit;
	}

	
	public void setDefaultTimeout(long timeout, TimeUnit defaultTimeoutUnit)
	{
		this.defaultTimeout = timeout;
		this.defaultTimeoutUnit = defaultTimeoutUnit;
	}

	@Override
	public void shutdown()
	{
		this.timeoutExecutor.shutdown();
		super.shutdown();
	}

	@Override
	public List<Runnable> shutdownNow()
	{
		this.timeoutExecutor.shutdownNow();
		return super.shutdownNow();
	}

	@Override
	protected void beforeExecute(Thread t, Runnable r)
	{
		long timeout = this.defaultTimeout;
		TimeUnit timeoutTimeunit = this.defaultTimeoutUnit;
		if (r instanceof TimeoutRunnable)
		{
			timeout = ((TimeoutRunnable) r).timeout();
			timeoutTimeunit = ((TimeoutRunnable) r).timeoutTimeunit();
		}
		if (timeout > 0)
		{
			final ScheduledFuture<?> scheduled = timeoutExecutor.schedule(new TimeoutTask(t), timeout, timeoutTimeunit);
			timeoutTasks.put(r, scheduled);
		}
		super.beforeExecute(t, r);
	}

	@Override
	protected void afterExecute(Runnable r, Throwable t)
	{
		ScheduledFuture<?> timeoutTask = timeoutTasks.remove(r);
		if (timeoutTask != null)
		{
			timeoutTask.cancel(false);
		}
		super.afterExecute(r, t);
	}

	private final class TimeoutTask implements Runnable
	{
		private final Thread thread;

		public TimeoutTask(Thread thread)
		{
			this.thread = thread;
		}

		@Override
		public void run()
		{
			thread.interrupt();
		}
	}
}