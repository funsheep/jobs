/*
	This file is part of the jobs library.
	Copyright (C) 2011-2013 Hendrik Renken

	This library is subject to the terms of the Mozilla Public License, v. 2.0.
	You should have received a copy of the MPL along with this library; see the
	file LICENSE. If not, you can obtain one at http://mozilla.org/MPL/2.0/.
*/
package cc.renken.jobs;

/**
 * Simple implementation of the {@link IDoJob} interface.
 * Only {@link #doJob()} is required for implementation. Then {@link #shutdown()} can be called to cancel, e.g. an endlessly looped job.
 * 
 * Another possibility is, to override {@link #length()} and {@link #absoluteProgress()}, then this job will be finished whenever
 * {@link #absoluteProgress()} is equal or larger to {@link #length()}.
 * 
 * @author Hendrik Renken
 */
public abstract class ADoJob implements IJob, IDoJob
{

	private final String name;
	/** Variable that indicates whether the {@link #shutdown()} method was called or not. Should not be written, but read only. */
	protected volatile boolean shutdown = false;


	/**
	 * Constructor that requires a name for the job.
	 * @param name The name of the job.
	 */
	public ADoJob(String name)
	{
		this.name = name;
	}

	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String name()
	{
		return this.name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long length()
	{
		return LENGTH_UNKNOWN;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long absoluteProgress()
	{
		return PROGRESS_UNKNOWN;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isfinished()
	{
		return this.shutdown || (this.length() != LENGTH_UNKNOWN && this.absoluteProgress() >= this.length());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void shutdown()
	{
		this.shutdown = true;
	}

}
