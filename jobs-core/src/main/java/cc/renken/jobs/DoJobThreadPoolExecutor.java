/**
 * 
 */
package cc.renken.jobs;

import java.util.concurrent.RunnableScheduledFuture;

import cc.renken.jobs.concurrent.DaemonizableThreadFactory;
import cc.renken.jobs.concurrent.TimeoutRunnable;
import cc.renken.jobs.concurrent.TimeoutThreadPoolExecutor;


/**
 * ExecutorService that monitors given tasks and interrupts them on a specified timeout. Further monitoring is not implemented.
 * If the monitored thread then still not ends itself - no further action is taken.
 * 
 * Within the constructor a default timeout can be set. This timeout is applied to any given task.
 * However, if the task implements the {@link TimeoutRunnable} interface, the default timeout is overridden with the values provided by the task.
 * 
 * A timeout value of zero or less disables the timeout mechanism. 
 * 
 * @author renkenh
 * @author Edward Dale (https://stackoverflow.com/questions/2758612/executorservice-that-interrupts-tasks-after-a-timeout)
 */
class DoJobThreadPoolExecutor extends TimeoutThreadPoolExecutor
{

	public DoJobThreadPoolExecutor(ExecutorConfiguration config)
	{
		super(config.getCorePoolSize(), new DaemonizableThreadFactory(config.getDaemonizeThreads()), config.getDefaultTaskTimeout(), config.getDefaultTaskTimeoutTimeUnit());
	}

	
	public void reconfigure(ExecutorConfiguration config)
	{
		this.setCorePoolSize(config.getCorePoolSize());
		this.setThreadFactory(new DaemonizableThreadFactory(config.getDaemonizeThreads()));
		this.setDefaultTimeout(config.getDefaultTaskTimeout(), config.getDefaultTaskTimeoutTimeUnit());
	}

	@Override
    protected <V> RunnableScheduledFuture<V> decorateTask(Runnable runnable, RunnableScheduledFuture<V> task)
	{
		return new DoJobScheduledFuture<>((IDoJob) runnable, task);
    }

}