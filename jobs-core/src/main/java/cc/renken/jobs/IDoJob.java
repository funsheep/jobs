/*
	This file is part of the jobs library.
	Copyright (C) 2011-2013 Hendrik Renken

	This library is subject to the terms of the Mozilla Public License, v. 2.0.
	You should have received a copy of the MPL along with this library; see the
	file LICENSE. If not, you can obtain one at http://mozilla.org/MPL/2.0/.
*/
package cc.renken.jobs;

/**
 * Job interface used to implement custom logic that is executed by the job platform. Custom logic can be implemented into the {@link #doJob()} method. This
 * method is called by the job platform for execution from the specific thread for which this job was scheduled.
 * A job may NOT block the execution. Instead, the {@link #doJob()} method MUST return within reasonable time. In the special case, that the method uses other
 * blocking APIs it must ensure that it can be interrupted. This is important for a clean shutdown of the job platform.
 * 
 * E.g. when reading a file, within the {@link #doJob()} method, the job could read as much bytes as there are available and then return. If nothing is available,
 * the job should return instantly. Given this example, a job USUALLY implements some kind of polling mechanism!
 * @author renkenh
 */
public interface IDoJob extends IJob
{

	/**
	 * The method that is called by the job platform to execute custom logic of the job. Exceptions and errors have to be handled within the job itself.
	 * If the job throws any exception, the exception is caught and the job is removed from the execution list (makes sense for looped jobs only).
	 */
	public void doJob() throws Exception;

}
