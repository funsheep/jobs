/**
 * 
 */
package cc.renken.jobs;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author renkenh
 *
 */
class DoTaskHandle<V> extends ADoJobHandle<V, IDoTask<V>>
{

	/**
	 * @param name
	 */
	public DoTaskHandle(IDoTask<V> job, String thread, long delay, long interExecTime, long timeout, TimeUnit unit)
	{
		super(job, thread, delay, interExecTime, timeout, unit);
	}


	@Override
	public void run()
	{
		try
		{
			V value = this.job.doTask();
			this.finished(value);
		}
		catch (InterruptedException ie)
		{
			this.finishedWithError(new TimeoutException("Job execution timeout"));
		}
		catch (Exception e)
		{
			this.finishedWithError(e);
		}
	}

}
