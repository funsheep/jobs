/**
 * 
 */
package cc.renken.jobs;

import java.util.concurrent.Delayed;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author renkenh
 *
 */
final class DoJobScheduledFuture<V> implements RunnableScheduledFuture<V>
{
	
	private final IDoJob job;
	private final RunnableScheduledFuture<V> task;


	/**
	 * 
	 */
	public DoJobScheduledFuture(IDoJob job, RunnableScheduledFuture<V> task)
	{
		this.job = job;
		this.task = task;
	}


	@Override
	public void run()
	{
		this.task.run();
	}


	@Override
	public boolean cancel(boolean mayInterruptIfRunning)
	{
		return this.task.cancel(mayInterruptIfRunning);
	}


	@Override
	public boolean isCancelled()
	{
		return this.task.isCancelled();
	}


	@Override
	public boolean isDone()
	{
		return this.task.isDone() || this.job.isfinished();
	}


	@Override
	public V get() throws InterruptedException, ExecutionException
	{
		return this.task.get();
	}


	@Override
	public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException
	{
		return this.task.get(timeout, unit);
	}


	@Override
	public long getDelay(TimeUnit unit)
	{
		return this.task.getDelay(unit);
	}

	@Override
	public int compareTo(Delayed o)
	{
		return (int) (o.getDelay(TimeUnit.MILLISECONDS) - this.getDelay(TimeUnit.MILLISECONDS));
	}

	@Override
	public boolean isPeriodic()
	{
		return this.task.isPeriodic();
	}

}
