/*
	This file is part of the jobs library.
	Copyright (C) 2011-2013 Hendrik Renken

	This library is subject to the terms of the Mozilla Public License, v. 2.0.
	You should have received a copy of the MPL along with this library; see the
	file LICENSE. If not, you can obtain one at http://mozilla.org/MPL/2.0/.
*/
package cc.renken.jobs;

/**
 * Main interface for jobs. A job that should be managed by the {@link Jobs} platform must implement this interface.
 * The interface provides methods for the platform to gain knowledge about the state of this job.
 * 
 * The job must have a name. The {@link #name()} must be unique within the executing thread.
 * 
 * The {@link #length()} method in
 * combination with the {@link #absoluteProgress()} provides information about the overall progress. However, those methods are
 * for informatory purposes only. For example, it does not matter when {@link #absoluteProgress()} returns a larger number than {@link #length()}.
 * 
 * A job is finished only if its {@link #isfinished()} method returns true. The job gets cleaned up at any moment after {@link #isfinished()} returned
 * true. The job is NOT informed about the cleanup by the Jobs platform.
 * 
 * On an eventual shutdown of the Jobs platform, on a job that is not finished, i.e. {@link #isfinished()} returns false, {@link #shutdown()} is by the platform called.
 * However, this method might also be called by some other mechanism - therefore, afterwards {@link #isfinished()} MUST return <code>true</code>.
 * 
 * Every method must be implemented thread save!
 * @author Hendrik Renken
 */
public interface IJob
{

	/** Constant to indicate that the length of the job is unknown. This is, e.g. the case for endlessly looped jobs. */
	public static final long LENGTH_UNKNOWN = -1;

	/** Constant to indicate that the progress is unknown. Usually, this is never the case. In almost all cases it is at least possible to provide the execution count. */
	public static final long PROGRESS_UNKNOWN = -1;


	/**
	 * The name of the job. Must be unique within the thread that executes this job.
	 * @return The name of the job.
	 */
	public String name();

	/**
	 * The absolute length of the job. The length does not have a specific unit and depends on the job. It could be the size of a file in bytes to be read. It also
	 * could be the number of executions this job will last. This method MUST be implemented thread save.
	 * @return The "length" of the job (no unit).
	 */
	public long length();

	/**
	 * The progress according to the unit of length. NOT in percentage! E.g. for a file to be read it could return the number of actually read bytes. Especially this
	 * method must be implemented thread save.
	 * @return The progress in {@link #length()} units.
	 */
	public long absoluteProgress();

	/**
	 * A job is finished only if this method returns <code>true</code>. It is up to the implementation to sync absoluteProgress/length with the return value of this method.
	 * However, this is optional. At any point in time after this method has returned <code>true</code>, the job is cleaned up. When set to <code>true</code> the value should
	 * not change anymore.
	 * In case, {@link #shutdown()} has been called, this method must return <code>true</code> afterwards.
	 * @return Whether the job is finished and ready for cleanup or not.
	 */
	public boolean isfinished();

	/**
	 * Is called in the case that {@link Jobs#shutdown()} is called and {@link #isfinished()} returns <code>false</code>. Therefore, this method must be thread save.
	 * After a call to this method, {@link #isfinished()} must return <code>true</code>.
	 * Within this call, a job can cleanup and free resources.
	 */
	public void shutdown();

}
