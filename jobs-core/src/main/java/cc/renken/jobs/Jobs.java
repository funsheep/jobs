/*
	This file is part of the jobs library.
	Copyright (C) 2011-2013 Hendrik Renken

	This library is subject to the terms of the Mozilla Public License, v. 2.0.
	You should have received a copy of the MPL along with this library; see the
	file LICENSE. If not, you can obtain one at http://mozilla.org/MPL/2.0/.
*/
package cc.renken.jobs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hub for all sorts of things regarding the scheduling and management of jobs in threads.
 * This type holds all registered jobs, allows the management of the jobs (add, remove, list). Furthermore, it transparently manages all threads that are needed
 * to execute the given jobs.
 * 
 * With this platform it is no longer needed to handle threads, their execution, interruption, or the executors for scheduling tasks. Furthermore, if two different
 * parts of a platform should run within the same thread (e.g. a plugin wants to have logic executed in the main thread), then references to executors or threads
 * must not be passed around.
 * Instead, a thread or executor is identified by its name. If the executor or thread is unknown yet, it will be created (and disposed) transparently.
 * 
 * The platform allows schedule custom jobs into arbitrary threads. Also, it provides the possibility to integrate external threads that cannot be ported to this'
 * platform API and mechanisms.
 * 
 * Jobs have to (at least) implement the {@link IJob} interface. However, to run custom logic within threads of this platform, {@link IDoJob} has to be implemented.
 * Jobs are then scheduled or registered with the method of this hub.
 * Jobs are put into a FIFS order if they are scheduled for the same point in time. With point in time also "run ASAP" (i.e. delay = 0) is meant.
 * 
 * In the usual case, all jobs are registered and handled through this platform. Technically, all internal threads are daemons. Therefore, with the end of the main
 * method of the java program, automatically all threads are halted and the JVM would exit. Therefore, the usual use case looks like this:
 * 
 * <pre> {@code
 * public static void main(String[] args)
 * {
 * 		//... start or register all needed jobs
 * 		Jobs.waitForShutdown();
 * }
 * }</pre>
 * 
 * Now it is possible to trigger {@link Jobs#shutdown()} from anywhere in the program - e.g. by the user in a UI. This would trigger {@link IJob#shutdown()} on every
 * currently running job and then the main method would exit gracefully.
 * 
 * 
 * 
 * @author Hendrik Renken
 */
public final class Jobs
{

	/** The name of the main thread. */
	public static final String MAIN_THREAD = "main";
	
	public static final int NO_DELAY = 0;
	public static final int NO_LOOP  = -1;
	public static final int NO_TIMEOUT = 0;

	private static final String SHUTDOWN_THREAD = MAIN_THREAD + "_maintenance";
	private static final int CLEANUP_RATE = 10; //sec
	private static final Logger LOGGER = LoggerFactory.getLogger(Jobs.class);
	
	/** Main lock to synchronize over main data structures. */
	private static final ReentrantLock LOCK_ALLJOBS = new ReentrantLock();
	private static final Condition WAIT_FOR_SHUTDOWN = LOCK_ALLJOBS.newCondition();

	private static final Set<IJobHandle<?>> ALL_JOBINFOS = new HashSet<>();
	private static final Map<String, DoJobThreadPoolExecutor> EXECUTORS_BY_NAME = new HashMap<>();
	protected static volatile boolean isShutdown = false;

	private static Map<String, ExecutorConfiguration> EXECUTOR_CONFIGS = new HashMap<>();
	private static ExecutorConfiguration DEFAULT_CONFIG = new ExecutorConfiguration();


	static
	{
		LOCK_ALLJOBS.lock();
		try
		{
			DoJobThreadPoolExecutor CLEANUP_EXECUTOR = new DoJobThreadPoolExecutor(ExecutorConfiguration.newSingletonThreadConfig());
			EXECUTORS_BY_NAME.put(SHUTDOWN_THREAD, CLEANUP_EXECUTOR);
			loopRunnable(SHUTDOWN_THREAD, Jobs::_cleanup, SHUTDOWN_THREAD, IDoJob.LENGTH_UNKNOWN, CLEANUP_RATE, CLEANUP_RATE, CLEANUP_RATE, TimeUnit.SECONDS);
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
	}

	
	public static final void setupExecutor(String thread, ExecutorConfiguration config)
	{
		LOCK_ALLJOBS.lock();
		try
		{
			config = new ExecutorConfiguration(config);
			EXECUTOR_CONFIGS.put(thread, config);
			DoJobThreadPoolExecutor exec = EXECUTORS_BY_NAME.get(thread);
			if (exec != null)
				exec.reconfigure(config);
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
	}

	public static final void setDefaultExecutorConfig(ExecutorConfiguration config)
	{
		LOCK_ALLJOBS.lock();
		try
		{
			DEFAULT_CONFIG = new ExecutorConfiguration(config);
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
	}


	/**
	 * Register the given job for management purposes only. The job will not be executed by this platform. Instead it will just be added to the set of known jobs.
	 * The creator of the job must execute/update this job all by himself. {@link IDoJob}s should not be registered here, instead they should be executed using
	 * {@link #runJob(IDoJob, String)}.
	 * @param job The job to register.
	 * @throws IllegalArgumentException If job is <code>null</code> or if the jobs name is <code>null</code>.
	 * @throws IllegalStateException If {@link Jobs#shutdown()} was called.
	 */
	public static void registerJob(IJob job)
	{
		registerJob(job, Thread.currentThread());
	}
	
	public static void registerJob(IJob job, Thread thread)
	{
		if (isShutdown)
			throw new IllegalStateException("Cannot add new jobs on or after shutdown.");
		if (job == null)
			throw new IllegalArgumentException("Job parameter may not be null");
		if (job.name() == null)
			throw new IllegalArgumentException("Job name may not be null");
		if (job instanceof IDoJob)
			LOGGER.warn("The job {} is a IDoJob and should not be registered. Instead it should be registered e.g. through runJob(). The job will not be executed", job.name());
		LOCK_ALLJOBS.lock();
		try
		{
			ALL_JOBINFOS.add(new RegisteredJobHandle(job, thread));
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
	}

	/**
	 * Registers a runnable for execution. The runnable will be executed in the given thread and will be executed as soon as possible.
	 * @param name The name of the job.
	 * @param run The runnable to execute.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @return The job hub for managing this job.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 * @throws IllegalStateException If {@link Jobs#shutdown()} was called.
	 */
	public static IJobHandle<Void> execute(String jobName, final Runnable run, String thread)
	{
		IDoJob job = new RunnableJob(jobName, run);
		return executeJob(job, thread);
	}

	/**
	 * Registers the given job for execution. The job will be executed by the given thread and will be executed as soon as possible.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 */
	public static IJobHandle<Void> executeJob(IDoJob job, String thread)
	{
		return loopJob(job, thread, NO_DELAY, NO_LOOP, NO_TIMEOUT, TimeUnit.MILLISECONDS);
	}

	/**
	 * Registers the given job for execution. The job will be executed by the given thread and will be executed as soon as possible.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 */
	public static IJobHandle<Void> executeJob(IDoJob job, String thread, long timeout, TimeUnit timeunit)
	{
		return loopJob(job, thread, NO_DELAY, NO_LOOP, timeout, timeunit);
	}

	/**
	 * Registers the given job for execution. The job will be executed by the given thread and will be executed as soon as futureMillis milliseconds have passed since registration.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @param futureMillis The delay until execution in milliseconds.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 * @throws IllegalStateException If {@link Jobs#shutdown()} was called.
	 */
	public static IJobHandle<Void> scheduleJob(IDoJob job, String thread, long futureMillis, TimeUnit unit)
	{
		return loopJob(job, thread, futureMillis, NO_LOOP, NO_TIMEOUT, unit);
	}

	/**
	 * Registers the given job for execution. The job will be executed by the given thread and will be executed as soon as futureMillis milliseconds have passed since registration.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @param futureMillis The delay until execution in milliseconds.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 * @throws IllegalStateException If {@link Jobs#shutdown()} was called.
	 */
	public static IJobHandle<Void> scheduleJob(IDoJob job, String thread, long futureMillis, long timeout, TimeUnit unit)
	{
		return loopJob(job, thread, futureMillis, NO_LOOP, timeout, unit);
	}

	/**
	 * Registers the given job for looped execution. The job will be executed by the given thread and will be executed as soon as possible. After execution, it will be rescheduled for execution.
	 * The platform tries to execute the job in such a way, that the every job start happens after interExecTime has passed - regardless of the time, the job actually needed. Of course, a job will
	 * never be execute twice at the same time.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @param interExecTime The time interval between two consecutive job starts in milliseconds.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 * @throws IllegalStateException If {@link Jobs#shutdown()} was called.
	 */
	public static IJobHandle<Void> loopJob(IDoJob job, String thread, long interExecTime, TimeUnit unit)
	{
		return loopJob(job, thread, NO_DELAY, interExecTime, NO_TIMEOUT, unit);
	}
	
	/**
	 * Registers the given job for looped execution. The job will be executed by the given thread and will be executed as soon as possible. After execution, it will be rescheduled for execution.
	 * The platform tries to execute the job in such a way, that the every job start happens after interExecTime has passed - regardless of the time, the job actually needed. Of course, a job will
	 * never be execute twice at the same time.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @param delay The initial delay until when the job should be executed for the first time
	 * @param interExecTime The time interval between two consecutive job starts in milliseconds.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 * @throws IllegalStateException If {@link Jobs#shutdown()} was called.
	 */
	public static IJobHandle<Void> loopRunnable(String jobName, Runnable run, String thread, long execCount, long delay, long interExecTime, long timeout, TimeUnit unit)
	{
		IDoJob job = new RunnableJob(jobName, execCount, run);
		return loopJob(job, thread, delay, interExecTime, timeout, unit);
	}
	
	/**
	 * Registers the given job for looped execution. The job will be executed by the given thread and will be executed as soon as possible. After execution, it will be rescheduled for execution.
	 * The platform tries to execute the job in such a way, that the every job start happens after interExecTime has passed - regardless of the time, the job actually needed. Of course, a job will
	 * never be execute twice at the same time.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @param delay The initial delay until when the job should be executed for the first time
	 * @param interExecTime The time interval between two consecutive job starts in milliseconds.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 * @throws IllegalStateException If {@link Jobs#shutdown()} was called.
	 */
	public static IJobHandle<Void> loopJob(IDoJob job, String thread, long delay, long interExecTime, long timeout, TimeUnit unit)
	{
		if (delay < 0)
			throw new IllegalArgumentException("Delay into the past not allowed.");
		if (interExecTime < NO_LOOP)
			throw new IllegalArgumentException("Looping into the past is not possible"); //except movie "Looper"
		if (isShutdown)
			throw new IllegalStateException("Cannot add new jobs on or after shutdown.");
		if (job == null)
			throw new IllegalArgumentException("Job parameter may not be null");
		if (job.name() == null)
			throw new IllegalArgumentException("Job name may not be null");
		if (thread == null)
			throw new IllegalArgumentException("Parameter thread may not be null");

		
		LOCK_ALLJOBS.lock();
		try
		{
			DoJobHandle info = new DoJobHandle(job, thread, delay, interExecTime, timeout, unit);
			ALL_JOBINFOS.add(info);

			DoJobThreadPoolExecutor djt = EXECUTORS_BY_NAME.get(thread);
			if (djt == null || djt.isShutdown())
			{
				ExecutorConfiguration config = EXECUTOR_CONFIGS.get(thread);
				if (config == null)
					config = DEFAULT_CONFIG;
				djt = new DoJobThreadPoolExecutor(config);
				EXECUTORS_BY_NAME.put(thread, djt);
			}
			if (interExecTime > 0)
				djt.scheduleAtFixedRate(info, Math.max(delay, 0), interExecTime, unit);
			else if (delay > 0)
				djt.schedule(info, delay, unit);
			else
				djt.execute(info);
			return info;
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
	}

	/**
	 * Registers a runnable for execution. The runnable will be executed in the given thread and will be executed as soon as possible.
	 * @param name The name of the job.
	 * @param run The runnable to execute.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @return The job hub for managing this job.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 * @throws IllegalStateException If {@link Jobs#shutdown()} was called.
	 */
	public static <VALUE> IJobHandle<VALUE> execute(String jobName, final Callable<VALUE> task, String thread)
	{
		IDoTask<VALUE> wrap = new CallableTask<>(jobName, task);
		return executeTask(wrap, thread);
	}

	/**
	 * Registers the given job for execution. The job will be executed by the given thread and will be executed as soon as possible.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 */
	public static <VALUE> IJobHandle<VALUE> executeTask(IDoTask<VALUE> task, String thread)
	{
		return loopTask(task, thread, NO_DELAY, NO_LOOP, NO_TIMEOUT, TimeUnit.MILLISECONDS);
	}

	/**
	 * Registers the given job for execution. The job will be executed by the given thread and will be executed as soon as possible.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 */
	public static <VALUE> IJobHandle<VALUE> executeTask(IDoTask<VALUE> task, String thread, long timeout, TimeUnit timeunit)
	{
		return loopTask(task, thread, NO_DELAY, NO_LOOP, timeout, timeunit);
	}

	/**
	 * Registers the given job for execution. The job will be executed by the given thread and will be executed as soon as futureMillis milliseconds have passed since registration.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @param futureMillis The delay until execution in milliseconds.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 * @throws IllegalStateException If {@link Jobs#shutdown()} was called.
	 */
	public static <VALUE> IJobHandle<VALUE> scheduleTask(IDoTask<VALUE> task, String thread, long futureMillis, TimeUnit unit)
	{
		return loopTask(task, thread, futureMillis, NO_LOOP, NO_TIMEOUT, unit);
	}

	/**
	 * Registers the given job for execution. The job will be executed by the given thread and will be executed as soon as futureMillis milliseconds have passed since registration.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @param futureMillis The delay until execution in milliseconds.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 * @throws IllegalStateException If {@link Jobs#shutdown()} was called.
	 */
	public static <VALUE> IJobHandle<VALUE> scheduleTask(IDoTask<VALUE> task, String thread, long futureMillis, long timeout, TimeUnit unit)
	{
		return loopTask(task, thread, futureMillis, NO_LOOP, timeout, unit);
	}

	/**
	 * Registers the given job for looped execution. The job will be executed by the given thread and will be executed as soon as possible. After execution, it will be rescheduled for execution.
	 * The platform tries to execute the job in such a way, that the every job start happens after interExecTime has passed - regardless of the time, the job actually needed. Of course, a job will
	 * never be execute twice at the same time.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @param interExecTime The time interval between two consecutive job starts in milliseconds.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 * @throws IllegalStateException If {@link Jobs#shutdown()} was called.
	 */
	public static <VALUE> IJobHandle<VALUE> loopTask(IDoTask<VALUE> task, String thread, long interExecTime, TimeUnit unit)
	{
		return loopTask(task, thread, NO_DELAY, interExecTime, NO_TIMEOUT, unit);
	}
	
	/**
	 * Registers the given job for looped execution. The job will be executed by the given thread and will be executed as soon as possible. After execution, it will be rescheduled for execution.
	 * The platform tries to execute the job in such a way, that the every job start happens after interExecTime has passed - regardless of the time, the job actually needed. Of course, a job will
	 * never be execute twice at the same time.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @param delay The initial delay until when the job should be executed for the first time
	 * @param interExecTime The time interval between two consecutive job starts in milliseconds.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 * @throws IllegalStateException If {@link Jobs#shutdown()} was called.
	 */
	public static <VALUE> IJobHandle<VALUE> loopCallable(String jobName, Callable<VALUE> task, String thread, long execCount, long delay, long interExecTime, long timeout, TimeUnit unit)
	{
		IDoTask<VALUE> job = new CallableTask<>(jobName, execCount, task);
		return loopTask(job, thread, delay, interExecTime, timeout, unit);
	}
	
	/**
	 * Registers the given job for looped execution. The job will be executed by the given thread and will be executed as soon as possible. After execution, it will be rescheduled for execution.
	 * The platform tries to execute the job in such a way, that the every job start happens after interExecTime has passed - regardless of the time, the job actually needed. Of course, a job will
	 * never be execute twice at the same time.
	 * @param job The job to register.
	 * @param thread The name of the thread in which this runnable should be executed.
	 * @param delay The initial delay until when the job should be executed for the first time
	 * @param interExecTime The time interval between two consecutive job starts in milliseconds.
	 * @throws IllegalArgumentException If thread, job or jobs name is <code>null</code>.
	 * @throws IllegalStateException If {@link Jobs#shutdown()} was called.
	 */
	public static <VALUE> IJobHandle<VALUE> loopTask(IDoTask<VALUE> task, String thread, long delay, long interExecTime, long timeout, TimeUnit unit)
	{
		if (delay < 0)
			throw new IllegalArgumentException("Delay into the past not allowed.");
		if (interExecTime < NO_LOOP)
			throw new IllegalArgumentException("Looping into the past is not possible"); //except movie "Looper"
		if (isShutdown)
			throw new IllegalStateException("Cannot add new jobs on or after shutdown.");
		if (task == null)
			throw new IllegalArgumentException("Task parameter may not be null");
		if (task.name() == null)
			throw new IllegalArgumentException("Task name may not be null");
		if (thread == null)
			throw new IllegalArgumentException("Parameter thread may not be null");

		
		LOCK_ALLJOBS.lock();
		try
		{
			DoTaskHandle<VALUE> info = new DoTaskHandle<>(task, thread, delay, interExecTime, timeout, unit);
			ALL_JOBINFOS.add(info);

			DoJobThreadPoolExecutor djt = EXECUTORS_BY_NAME.get(thread);
			if (djt == null || djt.isShutdown())
			{
				ExecutorConfiguration config = EXECUTOR_CONFIGS.get(thread);
				if (config == null)
					config = DEFAULT_CONFIG;
				djt = new DoJobThreadPoolExecutor(config);
				EXECUTORS_BY_NAME.put(thread, djt);
			}
			if (interExecTime > 0)
				djt.scheduleAtFixedRate(info, Math.max(delay, 0), interExecTime, unit);
			else if (delay > 0)
				djt.schedule(info, delay, unit);
			else
				djt.execute(info);
			return info;
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
	}

	/**
	 * Returns information about all known jobs.
	 * @return A collection containing all known jobs.
	 */
	public static Collection<IJobHandle<?>> getAllJobs()
	{
		LOCK_ALLJOBS.lock();
		try
		{
			return new HashSet<>(ALL_JOBINFOS);
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
	}
	
	public static void awaitTermination(String threadName) throws InterruptedException, TimeoutException
	{
		awaitTermination(threadName, DEFAULT_CONFIG.getDefaultTaskTimeout(), DEFAULT_CONFIG.getDefaultTaskTimeoutTimeUnit());
	}

	public static void awaitTermination(String threadName, long timeout, TimeUnit unit) throws InterruptedException, TimeoutException
	{
		DoJobThreadPoolExecutor exec = null;
		LOCK_ALLJOBS.lock();
		try
		{
			exec = EXECUTORS_BY_NAME.get(threadName);
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
		if (exec != null)
		{
			boolean success = exec.awaitTermination(timeout, unit);
			if (!success)
				throw new TimeoutException("Thread did not terminate before timeout.");
		}
	}

	public static void shutdownThread(String threadName)
	{
		DoJobThreadPoolExecutor exec = null;
		LOCK_ALLJOBS.lock();
		try
		{
			exec = EXECUTORS_BY_NAME.get(threadName);
			if (exec != null)
				exec.shutdown();
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
	}

	public static void forceShutdownThread(String threadName)
	{
		DoJobThreadPoolExecutor exec = null;
		LOCK_ALLJOBS.lock();
		try
		{
			exec = EXECUTORS_BY_NAME.get(threadName);
			if (exec != null)
				exec.shutdownNow();
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
	}

	/**
	 * Returns the information object about the given job.
	 * @param job The job for which to obtain information.
	 * @return An information object for the given job. If the job is unknown, <code>null</code> is returned.
	 */
	public static Collection<IJobHandle<?>> getJobHandles(IJob job)
	{
		LOCK_ALLJOBS.lock();
		try
		{
			return ALL_JOBINFOS.parallelStream().filter((i) -> i.getJob() == job).collect(Collectors.toList());
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
	}

	private static void _cleanup()
	{
		LOCK_ALLJOBS.lock();
		try
		{
			ALL_JOBINFOS.removeIf(IJobHandle::isDone);
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
		LOCK_ALLJOBS.lock();
		try
		{
			EXECUTORS_BY_NAME.values().removeIf((exec) -> exec.getPoolSize() == 0 && exec.getQueue().size() == 0);
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
	}

	/**
	 * Returns the amount of jobs known to the platform.
	 * @return The amount of jobs known.
	 */
	public static int jobCount()
	{
		return ALL_JOBINFOS.size();
	}

	/**
	 * Returns the name of the current thread as long, as the current thread belongs to this platform. If not, <code>null</code> is returned.
	 * @return The name of the current thread executed. <code>null</code> if the current thread is not managed by this platform.
	 */
	public static String getCurrentThread()
	{
		if (EXECUTORS_BY_NAME.containsKey(Thread.currentThread().getName()))
			return Thread.currentThread().getName();
		return null;
	}

	/**
	 * Shutdown this platform and all jobs.
	 */
	public static void shutdown()
	{
		isShutdown = true;
		Collection<DoJobThreadPoolExecutor> execs;
		LOCK_ALLJOBS.lock();
		try
		{
			execs = new ArrayList<>(EXECUTORS_BY_NAME.values());
			EXECUTORS_BY_NAME.clear();
			ALL_JOBINFOS.clear();
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
		try
		{
			execs.forEach(ScheduledExecutorService::shutdownNow);
		}
		finally
		{
			WAIT_FOR_SHUTDOWN.signalAll();
		}
	}
	
	/**
	 * Use this method to wait for the platform shutdown. Commonly, this method is used in the main() method - after initialization of the program - for the end of the program signaled by calling
	 * {@link #shutdown()}.
	 * @throws InterruptedException Is thrown if the current thread is interrupted.
	 */
	public static void waitForShutdown() throws InterruptedException
	{
		try
		{
			waitForShutdown(0);
		}
		catch (TimeoutException e)
		{
			throw new RuntimeException("Should not happen.");
		}
	}
	
	/**
	 * Use this method to wait for the platform shutdown. Commonly, this method is used in the main() method - after initialization of the program - for the end of the program signaled by calling
	 * {@link #shutdown()}.
	 * @param timeoutMillis The duration this method should block until it is exited or a {@link TimeoutException} is thrown. If set to zero, the method will wait indefinitely.
	 * @throws InterruptedException Is thrown if the current thread is interrupted.
	 * @throws TimeoutException If method takes longer to finish then allowed by given timeout duration. Is not thrown if timeout is set to zero.
	 */
	public static void waitForShutdown(int timeoutMillis) throws InterruptedException, TimeoutException
	{
		if (isShutdown)
			return;

		LOCK_ALLJOBS.lock();
		try
		{
			if (EXECUTORS_BY_NAME.containsKey(Jobs.getCurrentThread()))
				throw new RuntimeException("Managed thread cannot wait for shutdown. Would result in a dead lock.");

			while(!isShutdown)
			{
				if (timeoutMillis > 0 && !WAIT_FOR_SHUTDOWN.await(timeoutMillis, TimeUnit.MILLISECONDS))
					throw new TimeoutException();
				else
					WAIT_FOR_SHUTDOWN.await();
			}
		}
		finally
		{
			LOCK_ALLJOBS.unlock();
		}
	}

}
