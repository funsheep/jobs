/*
	This file is part of the jobs library.
	Copyright (C) 2011-2013 Hendrik Renken

	This library is subject to the terms of the Mozilla Public License, v. 2.0.
	You should have received a copy of the MPL along with this library; see the
	file LICENSE. If not, you can obtain one at http://mozilla.org/MPL/2.0/.
*/
package cc.renken.jobs;

import java.util.concurrent.TimeUnit;


/**
 * Internal data structure to hold information about a job. Furthermore, its {@link #toString()} method prints a pretty formated state of the job.
 * @author Hendrik Renken
 */
final class RegisteredJobHandle implements IJobHandle<Void>
{

	/** The job */
	private final IJob job;
	/** The thread within the referenced job is executed. */
	private final Thread thread;


	/**
	 * Internal constructor for the job platform.
	 */
	RegisteredJobHandle(IJob job, Thread thread)
	{
		this.job = job;
		this.thread = thread;
	}


	@Override
	public boolean isDone()
	{
		return this.job.isfinished();
	}

	@Override
	public void cancel()
	{
		this.job.shutdown();
	}

	@Override
	public void shutdown()
	{
		this.job.shutdown();
		this.thread.interrupt();
	}

	public IJob getJob()
	{
		return this.job;
	}

	public long getDelay()
	{
		return 0;
	}

	public boolean isLoop()
	{
		return false;
	}

	public String getThread()
	{
		return this.thread.getName();
	}
	
	@Override
	public long interExecutionTime()
	{
		return 0;
	}

	@Override
	public long timeout()
	{
		return 0;
	}

	@Override
	public TimeUnit timeUnit()
	{
		return TimeUnit.MILLISECONDS;
	}

	@Override
	public Void tryGet()
	{
		return this.get(0);
	}


	@Override
	public Void get(long timeout)
	{
		throw new UnsupportedOperationException("Unsupported operation for registered jobs.");
	}
	
	@Override
	public String toString()
	{
		return JobInfoUtil.toString(this);
	}
	
}
