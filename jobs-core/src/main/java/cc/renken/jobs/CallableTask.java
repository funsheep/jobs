/*
	This file is part of the jobs library.
	Copyright (C) 2011-2013 Hendrik Renken

	This library is subject to the terms of the Mozilla Public License, v. 2.0.
	You should have received a copy of the MPL along with this library; see the
	file LICENSE. If not, you can obtain one at http://mozilla.org/MPL/2.0/.
*/
package cc.renken.jobs;

import java.util.concurrent.Callable;

/**
 * Adapter between {@link Runnable} and {@link IDoJob}.
 * Takes a Runnable and executes it a defined number of times - or endlessly.
 * 
 * Upon creation, a number of times the runnable should be executed can be provided. Every time the runnable is executed, the counter
 * rises by one. Eventually, the counter will reach the number of executions. Then the job will return for {@link #isfinished()} <code>true</code>.
 * Then the scheduling of the job is stopped and the job is eventually cleaned up. If {@link IJob#LENGTH_UNKNOWN} is provided as the number
 * of execution times, the job is executed endlessly. Of course, this will work if and only if the job is scheduled for looping through
 * {@link Jobs#loopJob(IDoJob, String, long)}.
 * 
 * @author Hendrik Renken
 */
public final class CallableTask<V> implements IDoTask<V>
{

	private final String name;
	private final long length;
	private final Callable<V> call;
	private volatile long count = 0;
	private volatile boolean shutdown = false;


	/**
	 * Constructor. Takes the runnable and executes it exactly once.
	 * Instead of this constructor also {@link Jobs#runJob(String, Runnable, String)} may be used.
	 * @param name The name of the job.
	 * @param run The runnable to execute.
	 */
	public CallableTask(String name, Callable<V> call)
	{
		this(name, 1, call);
	}

	/**
	 * Constructor. Takes the runnable and executes it the given number of times. But ONLY, if the job is a {@link Jobs#loopJob(IDoJob, String, long)}.
	 * @param name The name of the job.
	 * @param execCount The number of times the job should be executed until it is finished.
	 * @param run The runnable to execute.
	 */
	public CallableTask(String name, long execCount, Callable<V> call)
	{
		this.name = name;
		this.call = call;
		this.length = execCount;
	}


	@Override
	public String name()
	{
		return this.name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long absoluteProgress()
	{
		return this.count;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long length()
	{
		return this.length;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isfinished()
	{
		return this.shutdown || (this.length() != LENGTH_UNKNOWN && this.absoluteProgress() >= this.length());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void shutdown()
	{
		this.shutdown = true;
	}

	@Override
	public V doTask() throws Exception
	{
		return this.call.call();
	}

}
