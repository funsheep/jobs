package cc.renken.jobs;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public interface IJobHandle<V>
{

	/**
	 * @return the job
	 */
	public IJob getJob();

	/**
	 * @return the delay
	 */
	public long getDelay();

	public long interExecutionTime();
	
	/**
	 * @return the loop
	 */
	public default boolean isLooped()
	{
		return this.interExecutionTime() > 0;
	}

	public long timeout();
	
	public default boolean hasTimeout()
	{
		return this.timeout() > 0;
	}

	public TimeUnit timeUnit();
	
	/**
	 * @return the thread
	 */
	public String getThread();

	
	public boolean isDone();
	

	/**
	 * Same as {@link #get(int)} with timeout set to zero. This method will wait indefinitely until the job has finished.
	 * @return The result that was handed over by the producer by calling {@link #finished(Object)}.
	 * @throws Exception Rethrows the exception that was handed over by the producer by calling {@link #finishedWithError(Exception)}.
	 */
	public default V get() throws TimeoutException, Exception
	{
		return this.get(this.timeout());
	}
	
	/**
	 * Returns the result or throws the exception or <code>null</code> (if there is no result) without blocking. Tries to get the result (or trigger the exception) if there is
	 * some (i.e. {@link #isfinished()} returns <code>true</code>). Otherwise, will return <code>null</code>. Therefore, it does not make sense to use this method
	 * if <code>null</code> is a valid return value - because you cannot distinct it from each other.
	 * @return The result if there is one, <code>null</code> otherwise.
	 * @throws Exception Is thrown if the job {@link #finishedWithError(Exception)}.
	 */
	public V tryGet() throws Exception;

	/**
	 * The method for the consumer to get the result of the producer. The method will block until the producer either called {@link #finished(Object)} or
	 * {@link #finishedWithError(Exception)}. If the producer calls the latter one, the exception is thrown to the consumer. It has to catch it.
	 * If timeout is greater then zero, the method will block until the time has elapsed only and throw a {@link TimeoutException}.
	 * @return The result that was handed over by the producer by calling {@link #finished(Object)}.
	 * @throws TimeoutException Is thrown if the given duration elapses detectably before the job finishes with a result or error.
	 * @throws Exception Rethrows the exception that was handed over by the producer by calling {@link #finishedWithError(Exception)}.
	 */
	public V get(long timeout) throws TimeoutException, Exception;

	
	public void cancel();
	
	public void shutdown();

}