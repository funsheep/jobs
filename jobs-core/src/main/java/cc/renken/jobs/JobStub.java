/*
	This file is part of the jobs library.
	Copyright (C) 2011-2013 Hendrik Renken

	This library is subject to the terms of the Mozilla Public License, v. 2.0.
	You should have received a copy of the MPL along with this library; see the
	file LICENSE. If not, you can obtain one at http://mozilla.org/MPL/2.0/.
*/
package cc.renken.jobs;


/**
 * Full blown implementation of the IJob interface for unmanaged threads.
 * 
 * The stub registers itself at the platform. Then it can be controlled from there (e.g. {@link #shutdown()} can be called. In an optimal way,
 * the unmanaged thread uses this stub for synchronization. It can control the length, the absolute progress (in terms of length) and it SHOULD
 * set {@link #isfinished()} through {@link #finish()} to give the platform the possibility for a cleanup.
 * Furthermore, it SHOULD check {@link #isfinished()} regularly to get information about a possible shutdown. I.E. the thread should end its work when {@link #isfinished()}
 * returns true.
 * However, if a thread is provided when this object is created, upon calling {@link #shutdown()} the thread is interrupted through {@link Thread#interrupt()}.
 * 
 * @author Hendrik Renken
 */
public final class JobStub implements IJob
{

	private final String name;
	private long overallLength = IJob.LENGTH_UNKNOWN;
	private long done = IJob.PROGRESS_UNKNOWN;
	private volatile boolean finished;
	private final Thread jobthread;


	/**
	 * Simple constructor.
	 * Length is set to {@link IJob#LENGTH_UNKNOWN}.
	 * The current thread is managed and the job is named after the current thread.
	 */
	public JobStub()
	{
		this(IJob.LENGTH_UNKNOWN);
	}
	
	/**
	 * Simple constructor. The name of the job must be given.
	 * Length is set to {@link IJob#LENGTH_UNKNOWN}.
	 * No thread will be managed by this job.
	 * @param name The name of the job.
	 */
	public JobStub(String name)
	{
		this(name, IJob.LENGTH_UNKNOWN);
	}
	
	/**
	 * Constructor. The length must be given.
	 * The current thread is managed and the job is named after the current thread.
	 * @param length The length of the job.
	 */
	public JobStub(long length)
	{
		this(length, Thread.currentThread());
	}

	/**
	 * Constructor. The job is named after the managed thread.
	 * @param length The length of the job.
	 * @param jobthread The thread to manage. May not be <code>null</code>.
	 */
	public JobStub(long length, Thread jobthread)
	{
		this(jobthread.getName(), length, jobthread);
	}
	
	/**
	 * Constructor. Name and length must be given.
	 * This stub will not manage any thread.
	 * @param name The name of the job.
	 * @param length The length of the job.
	 */
	public JobStub(String name, long length)
	{
		this(name, length, null);
	}
	
	private JobStub(String name, long length, Thread jobthread)
	{
		this.overallLength = length;
		this.name = name;
		this.jobthread = jobthread;
		Jobs.registerJob(this);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public String name()
	{
		return this.name;
	}

	/**
	 * Set the done value to the specified value.
	 * This method is thread safe.
	 * @param count The value of done in terms of length units.
	 */
	public synchronized void setDone(long count)
	{
		if (this.done == IJob.PROGRESS_UNKNOWN)
			this.done = 0;
		this.done = count;
		if (this.overallLength != LENGTH_UNKNOWN)
			this.done = Math.min(count, this.overallLength);
	}

	/**
	 * Adds the given value to the current {@link #absoluteProgress()} value.
	 * This method is thread safe.
	 * @param addCount The "amount of done" to add.
	 */
	public synchronized void addDone(long addCount)
	{
		if (this.done == IJob.PROGRESS_UNKNOWN)
			this.done = 0;
		this.setDone(this.absoluteProgress() + addCount);
	}

	/**
	 * Increases the amount of {@link #absoluteProgress()} by one.
	 * This method is thread safe.
	 */
	public synchronized void incDone()
	{
		this.addDone(1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized long absoluteProgress()
	{
		return this.done;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized long length()
	{
		return this.overallLength;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isfinished()
	{
		return this.finished || (this.length() != LENGTH_UNKNOWN && this.length() == this.absoluteProgress());
	}

	
	/**
	 * Marks this thread as finished. The platform may then cleanup the thread at any time.
	 * This will NOT interrupt the managed thread. Only the managed thread should call this method and
	 * then it should stop working.
	 * This method is thread safe.
	 */
	public synchronized void finish()
	{
		this.finished = true;
		if (this.length() != LENGTH_UNKNOWN)
			this.setDone(this.length());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void shutdown()
	{
		this.finished = true;
		if (this.jobthread != null && !this.jobthread.isInterrupted() && this.jobthread != Thread.currentThread())
			this.jobthread.interrupt();
	}

}
