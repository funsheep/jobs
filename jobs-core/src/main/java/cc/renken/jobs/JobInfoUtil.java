/**
 * 
 */
package cc.renken.jobs;

import java.text.NumberFormat;

/**
 * @author renkenh
 *
 */
public final class JobInfoUtil
{

	public static final String toString(IJobHandle<?> info)
	{
		StringBuilder sb = new StringBuilder(20);
		sb.append(info.isLooped() ? '\u21BB' : ' ');
		sb.append(info.getJob().name());
		sb.append(" \t");
		sb.append('[');
		if (info.getJob().isfinished())
		{
			sb.append("100%");
		}
		else if (info.getJob().length() != IJob.LENGTH_UNKNOWN && info.getJob().length() > 0 && info.getJob().absoluteProgress() != IJob.PROGRESS_UNKNOWN)
		{
			NumberFormat nf = NumberFormat.getPercentInstance();
			sb.append(nf.format((float) info.getJob().absoluteProgress() / info.getJob().length()));
		}
		else if (info.getJob().absoluteProgress() != IJob.PROGRESS_UNKNOWN)
			sb.append(info.getJob().absoluteProgress());
		else
			sb.append('!');
		sb.append(']');
		if (info.getThread() != null)
		{
			sb.append(" \tin '");
			sb.append(info.getThread());
			sb.append('\'');
		}
		return sb.toString();
	}



	private JobInfoUtil()
	{
		//no instance
	}

}
