/*
	This file is part of the jobs library.
	Copyright (C) 2011-2013 Hendrik Renken

	This library is subject to the terms of the Mozilla Public License, v. 2.0.
	You should have received a copy of the MPL along with this library; see the
	file LICENSE. If not, you can obtain one at http://mozilla.org/MPL/2.0/.
*/
package cc.renken.jobs;


/**
 * Adapter between {@link Runnable} and {@link IDoJob}.
 * Takes a Runnable and executes it a defined number of times - or endlessly.
 * 
 * Upon creation, a number of times the runnable should be executed can be provided. Every time the runnable is executed, the counter
 * rises by one. Eventually, the counter will reach the number of executions. Then the job will return for {@link #isfinished()} <code>true</code>.
 * Then the scheduling of the job is stopped and the job is eventually cleaned up. If {@link IJob#LENGTH_UNKNOWN} is provided as the number
 * of execution times, the job is executed endlessly. Of course, this will work if and only if the job is scheduled for looping through
 * {@link Jobs#loopJob(IDoJob, String, long)}.
 * 
 * @author Hendrik Renken
 */
public final class RunnableJob extends ADoJob implements IDoJob
{

	private final long length;
	private final Runnable run;
	private volatile long count = 0;


	/**
	 * Constructor. Takes the runnable and executes it exactly once.
	 * Instead of this constructor also {@link Jobs#runJob(String, Runnable, String)} may be used.
	 * @param name The name of the job.
	 * @param run The runnable to execute.
	 */
	public RunnableJob(String name, Runnable run)
	{
		this(name, 1, run);
	}

	/**
	 * Constructor. Takes the runnable and executes it the given number of times. But ONLY, if the job is a {@link Jobs#loopJob(IDoJob, String, long)}.
	 * @param name The name of the job.
	 * @param execCount The number of times the job should be executed until it is finished.
	 * @param run The runnable to execute.
	 */
	public RunnableJob(String name, long execCount, Runnable run)
	{
		super(name);
		this.run = run;
		this.length = execCount;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public long absoluteProgress()
	{
		return this.count;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doJob()
	{
		this.run.run();
		this.count++;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long length()
	{
		return this.length;
	}

}
