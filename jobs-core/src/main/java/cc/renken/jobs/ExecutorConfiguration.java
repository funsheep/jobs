/**
 * 
 */
package cc.renken.jobs;

import java.util.concurrent.TimeUnit;

/**
 * @author renkenh
 *
 */
public class ExecutorConfiguration
{
	
	private int coreSize = 0;
	private boolean daemonThreads = true;
	private long defaultTaskTimeout = 0;
	private TimeUnit defaultTaskTimeoutTimeUnit = TimeUnit.MILLISECONDS;

	
	public ExecutorConfiguration()
	{
		//do nothing
	}
	
	ExecutorConfiguration(ExecutorConfiguration config)
	{
		this.setCorePoolSize(config.getCorePoolSize())
			.setDaemonizeThreads(config.getDaemonizeThreads())
			.setDefaultTaskTimeout(config.getDefaultTaskTimeout())
			.setDefaultTaskTimeoutTimeUnit(config.getDefaultTaskTimeoutTimeUnit());
	}
	
	
	public int getCorePoolSize()
	{
		return coreSize;
	}
	
	public ExecutorConfiguration setCorePoolSize(int coreSize)
	{
		this.coreSize = coreSize;
		return this;
	}
	
	public ExecutorConfiguration setDaemonizeThreads(boolean daemonize)
	{
		this.daemonThreads = daemonize;
		return this;
	}
	
	public boolean getDaemonizeThreads()
	{
		return this.daemonThreads;
	}
	
	public long getDefaultTaskTimeout()
	{
		return defaultTaskTimeout;
	}
	
	public ExecutorConfiguration setDefaultTaskTimeout(long defaultTaskTimeout)
	{
		this.defaultTaskTimeout = defaultTaskTimeout;
		return this;
	}
	
	public TimeUnit getDefaultTaskTimeoutTimeUnit()
	{
		return defaultTaskTimeoutTimeUnit;
	}
	
	public ExecutorConfiguration setDefaultTaskTimeoutTimeUnit(TimeUnit defaultTaskTimeoutTimeUnit)
	{
		this.defaultTaskTimeoutTimeUnit = defaultTaskTimeoutTimeUnit;
		return this;
	}
	
	
	public static final ExecutorConfiguration newSingletonThreadConfig()
	{
		return newFixedThreadPoolConfig(1);
	}
	
	public static final ExecutorConfiguration newFixedThreadPoolConfig(int nThreads)
	{
		return new ExecutorConfiguration().setCorePoolSize(nThreads);
	}
}
